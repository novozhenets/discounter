module Rule
  def self.find(code)
    Storage.rules.find { |rule| rule.code?(code) }
  end

  class Base
    attr_reader :code

    def self.create(code, required_products, discounted_product_code, discounted_price)
      Storage.rules << new(code, required_products, discounted_product_code, discounted_price)
    end

    def initialize(code, required_products, discounted_product_code, discounted_price)
      @code = code
      @required_products = required_products
      @discounted_product_code = discounted_product_code
      @discounted_price = discounted_price
    end

    def apply(items)
      return unless valid?(items)

      execute_rule(items)
    end

    def code?(check_code)
      code == check_code
    end

    private

    attr_reader :required_products, :discounted_product_code, :discounted_price

    def execute_rule
      raise NotImplementedError
    end

    def valid?(items)
      non_discounted_items = items.reject(&:discounted?)
      non_discounted_items.map(&:code).uniq.all? do |x|
        non_discounted_items.count { |i| i.code?(x) } >= required_products.count(x)
      end
    end
  end
end
