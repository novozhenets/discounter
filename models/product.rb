class Product
  class << self
    def create(name:, code:, price:)
      Storage.products << new(name, code, price)
    end

    def find(code)
      Storage.products.find { |product| product.code?(code) }
    end
  end

  attr_reader :name, :code, :price

  def initialize(name, code, price)
    @name = name
    @code = code
    @price = price
  end

  def code?(check_code)
    code == check_code
  end
end
