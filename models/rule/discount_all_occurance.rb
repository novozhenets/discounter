module Rule
  class DiscountAllOcurrance < Base
    private

    def execute_rule(items)
      items.select { |x| x.code?(discounted_product_code) }.each do |item|
        item.apply_discount(discounted_price) unless item.discounted?
      end
    end
  end
end
