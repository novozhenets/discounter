module Rule
  class GetBonusItem < Base
    private

    def execute_rule(items)
      items.select { |x| !x.discounted? && x.code?(discounted_product_code) }.each_slice(2) do |batch|
        batch[1].apply_discount(discounted_price) if batch[1]
      end
    end

    def discounted_product
      @discounted_product ||= Product.find(discounted_product_code)
    end
  end
end
