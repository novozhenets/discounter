class Cart
  @items = []
  @rules = []

  class << self
    def add_rules_menu
      puts "Enter rule codes list, separated by comma. Existing rules: #{Storage.rules.map(&:code).join(', ')}"
      draw_menu do |code|
        rule = Rule.find(code)
        rules << rule if rule
      end
    end

    def add_products_menu
      puts "Enter product codes list, separated by comma. Existing products: #{Storage.products.map(&:code).join(', ')}"
      draw_menu do |code|
        product = Product.find(code)
        items << CartItem.new(product.name, product.code, product.price) if product
      end
    end

    def show
      return 'Cart is empty' if items.empty?

      apply_discount_rules

      result = "Items in cart:\n\n"
      result += items.map.each_with_index do |item, index|
        item_result = "#{index + 1}. #{item.name}, price: $#{item.price}"
        item_result += ", price after discount: $#{item.discounted_price}" if item.discounted?
        item_result
      end.join("\n")
      result += "\nTotal cost: $#{total_cost}"
      result += "\nCost after discount: $#{discounted_cost}" if items.any?(&:discounted?)
      result += "\nDiscount rules: #{rules.map(&:code).join(', ')}" unless rules.empty?
      result
    end

    def total_cost
      items.map(&:price).sum.round(2)
    end

    def discounted_cost
      items.map(&:discounted_price).sum.round(2)
    end

    def clear
      @items = []
      @rules = []
      'Cart cleared'
    end

    private

    attr_reader :items, :rules

    def draw_menu
      added_codes = []
      wrong_codes = []
      codes = gets.chomp.split(',').map { |code| code.gsub(/\s+/, '') }
      codes.each do |code|
        if yield(code)
          added_codes << code
        else
          wrong_codes << code
        end
      end

      result = []
      result << "Code(s) #{added_codes.join(', ')} added" unless added_codes.empty?
      result << "#{wrong_codes.join(', ')} skipped as wrong code(s)" unless wrong_codes.empty?
      result.join(', ')
    end

    def apply_discount_rules
      items.each(&:reset_discount!)
      rules.each { |rule| rule.apply(items) }
    end
  end
end
