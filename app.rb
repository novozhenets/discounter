Dir[File.dirname(__FILE__) + '/models/**/*.rb'].sort.each { |file| require file }
Dir[File.dirname(__FILE__) + '/lib/*.rb'].sort.each { |file| require file }

class App
  def initialize
    Seed.setup
    clear_screen
  end

  def run
    error = nil
    result = nil

    loop do
      puts "!!! #{error} !!!\n\n" if error
      puts "#{result}\n\n" if result
      puts 'Select menu item:'
      main_menu = {
        '1' => ['Add product to cart', Cart.method(:add_products_menu)],
        '2' => ['Add rules to cart', Cart.method(:add_rules_menu)],
        '3' => ['Show cart', Cart.method(:show)],
        '4' => ['Clear cart', Cart.method(:clear)],
        '5' => ['Exit', method(:exit_app)]
      }
      main_menu.each do |index, item|
        puts "#{index}. #{item[0]}"
      end
      command = gets.chomp
      if main_menu.key?(command)
        result = main_menu[command][1].call
      else
        error = command.gsub(/\s+/, '').empty? ? 'Please select menu item' : "There is no #{command} option in the menu"
      end
      clear_screen
    end
  end

  def clear_screen
    system('clear') || system('cls')
  end

  def exit_app
    clear_screen
    exit
  end
end

App.new.run
